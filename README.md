# webapptest

Aplicación fullstack de forma local, dónde el front-end será en Angular 6+ y el back-end en NodeJs 10+. Para la base de datos
se podría usar MySQL

## Versiones

Angular CLI: 11.0.3
Node: 12.14.0
Mysql 15.1 Distrib 10.1.13-MariaDB, for Win32 (AMD64)

## Getting started

No se usan script para crear la BD

Debe crearse manualmente (mysql) con el nombre (webapptest)

## Dentro de la carpeta (api/) 
ejecutar el comando npm install

ejecutar el comando npm run refresh (esto permite limpiar la BD y cargar las migraciones)

ejecutar el comando npm run start (para correr el api)

## Dentro de la carpeta (web/) 

ejecutar el comando npm install

ejecutar el comando npm run start (para levantar el servidor)

ejecutar el comando npm run build (para compilar a producción)


