"use strict";
const { Modelo } = require("./Modelo");

class Employees extends Modelo {
  static get tableName() {
    return "employees";
  }
}

module.exports = Employees;
