class ApiResponser {
    static SendData(data, res) {
        return res.status(200).send(data);
    }

    static SendMessage(msg, res) {
        return res.status(200).send({
            msg: msg,
            code: 200
        });
    }

    static SendAuth(res, status, token = null, user = null) {
        if (!status)
            res.status(401).send({
                auth: false,
                token: null
            });
        else
            res.status(200).send({
                auth: true,
                token: token,
                user: user
            });
    }

    static SendError(m, res, code) {
        return res.status(code).send({
            msg: m
        });
    }
}

module.exports = ApiResponser;
