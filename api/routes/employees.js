const Employees = require('../models/Employees');

const validate = require('./interceptor');
const AR = require('../ApiResponser');
const upload = require('multer')();
const router = require('express').Router();
const sanitizeString = require('../functions/sanitizeString');

const
  HTTP_BAD_REQUEST = 400,
  HTTP_NOT_FOUND = 404,
  HTTP_SERVER_ERROR = 500

router.get('/get/:page/:items/:filter?', /*validate('employees'),*/ async(req, res) => {
  const params = req.params;

  const Query = Employees.query();

    Query.where("status", '=', 0);
  if (+params.page === +params.page)
    Query.page(parseInt(params.page), +params.items ? parseInt(params.items) : 10)

  if (params.filter)
    if (String(params.filter).length > 1) {
      Query.having('name', 'LIKE', '%'+sanitizeString(params.filter)+'%');
      Query.orHaving('username', 'LIKE', '%'+sanitizeString(params.filter)+'%');
    }
    
  await Query
    .then(resp => AR.SendData(resp, res))
    .catch(err => {
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.get('/analytics', /*validate('employees'),*/ async(req, res) => {

  const Query = Employees.query();
  Query.select("age");
  Query.count('age', {as: 'total'});
  Query.where('status', '=', 0);
  Query.groupBy("age");
  
  await Query
    .then(resp => {
      let data = {
        labels: [],
        totals: []
      };
      for (let index = 0; index < resp.length; index++) {
        const e = resp[index];
        data.labels.push(`Age ${e.age}`);
        data.totals.push(e.total);
      }
      AR.SendData(data, res)
    })
    .catch(err => {
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});



router.post('/add', upload.none(), /*validate('employees'),*/ async(req, res) => {
  const data = req.body;

  let errr = false;
  if (!data.name) errr = "Name is required";
  else if (!data.age) errr = "Age is required";
  else if (!data.username) errr = "Username is required";

  if (errr)
    return AR.SendError(errr, res, HTTP_BAD_REQUEST);


  const Set = {
    name: data.name,
    age: data.age,
    username: data.username
  };


  await Employees.query()
    .insert(Set)
    .then(async resp => {
      if (!resp)
        return res.sendStatus(HTTP_NOT_FOUND);
      resp.r = true;
      resp.msg = "Success";
      AR.SendData(resp, res);
    })
    .catch(err => {
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.put('/edit', upload.none(), /*validate('employees'),*/ async(req, res) => {
  const data = req.body;
  let errr = false;
  if (!data.id) errr = "Id is required";
  else if (!data.name) errr = "Name is required";
  else if (!data.age) errr = "Age is required";
  else if (!data.username) errr = "Username is required";

  if (errr)
    return AR.SendError(errr, res, HTTP_BAD_REQUEST);

  const Set = {
    name: data.name,
    age: data.age,
    username: data.username
  };

  await Employees.query()
    .patchAndFetchById(data.id, Set)
    .then(async resp => {
      if (!resp)
        return res.sendStatus(HTTP_NOT_FOUND);
      resp.r = true;
      resp.msg = "Success";
      AR.SendData(resp, res);
    })
    .catch(err => {
      res.sendStatus(HTTP_SERVER_ERROR);
    })
});

router.delete('/delete/:id(\\d+)', /*validate('employees'),*/ async(req, res) => {
  const data = req.params;
  const Set = {
    status: 1,
  };
  await Employees.query()
  .patchAndFetchById(data.id, Set)
  .then(async resp => {
    if (!resp)
      return res.sendStatus(HTTP_NOT_FOUND);
    resp.r = true;
    resp.msg = "Success";
    AR.SendData(resp, res);
  })
  .catch(err => {
    res.sendStatus(HTTP_SERVER_ERROR);
  })
});

module.exports = router;
