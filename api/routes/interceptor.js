const { knex } = require("objection");

const MODELS = {
  employees: require("../models/Employees"),
}

const TABLES = Object.values(MODELS).map(modelo => modelo.tableName);

function unionAllTokens(builder){
  for (let table of TABLES)
    builder.unionAll(q => q.select('token').from(table));
}

async function validator(ModelUser, token) {
    const users = await ModelUser.query().count('* AS count').first().where({token}).catch(console.log);

    if(users && users.count > 0){
      return true
    }

    return false;
}

async function validator_global(token) {
  return await knex.select().from({tokens: unionAllTokens}).where('token', '=', token).first();
}

module.exports = function sesion2(...with_access) {
  return async function (req, res, next) {
    const bearerHeader = req.headers['authorization'];
    let token = undefined;
    if (bearerHeader) {
      const bearer = bearerHeader.split(' ');
      const bearerToken = bearer[1];
      token = bearerToken;
    }
    if (!token) return res.sendStatus(401);

    if(with_access.length === 0 || with_access[0] == 'general') {
      let acceso = await validator_global(token);
      if (acceso) return next();
    }
    else {
      for (type_users of with_access) {
        let acceso = await validator(MODELS[type_users], token);
        if (acceso) {
          return next();
        }
      }
    }

    res.sendStatus(401);
  }
}
