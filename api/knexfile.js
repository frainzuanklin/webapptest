// Update with your config settings.

module.exports = {
  PORT: 3000,
  Hosname: "server.dev",
  development: {
    client: 'mysql',
    connection: {
      database: 'webapptest',
      user:     'root',
      password: '',
      host: 'localhost'
    },
    migrations: {
        directory: "./migrations"
    },
    seeds: {
      directory: "./seeds"
    }
  },

  production: {
    client: 'mysql',
    connection: {
      database: 'webapptest',
      user:     'root',
      password: '',
      host: 'localhost'
    },
    migrations: {
        directory: "./migrations"
    },
    seeds: {
      directory: "./seeds"
    }
  }

};
