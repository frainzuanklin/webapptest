const os = require("os");
const Server = os.hostname();
const knexConfig = require("./knexfile");
if (Server == knexConfig.Hosname) {
  var dominio = "https://server.dev:" + knexConfig.PORT;
  var dominioWeb = "https://server.dev/";
} else {
  var dominio = "http://localhost:" + knexConfig.PORT;
  var dominioWeb = "http://localhost/";

}
module.exports = {
  secret: "Clave@#2",
  dominio: dominio,
  dominioWeb: dominioWeb,
  rutaArchivo: function(archivo) {
    return this.dominio + "\\public\\uploads\\" + archivo;
  },
  rutaWeb: function(ruta) {
    return this.dominioWeb + ruta;
  },
};
