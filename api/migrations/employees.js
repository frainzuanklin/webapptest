
exports.up = function(knex) {
  return knex.schema.createTable('employees', table => {
    table.increments('id').primary();
    table.string('name', 100);
    table.integer('age');
    table.string('username', 50);
    table.integer('status').defaultTo(0);
  });
};

exports.down = function(knex) {
  return knex.schema.dropTableIfExists('employees');
};
