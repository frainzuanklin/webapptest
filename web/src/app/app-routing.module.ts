import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { EmployeesComponent } from './views/employees/employees.component';


const routes: Routes = [
    {
      path: '',
      children: [
        { path: 'dashboard', component: DashboardComponent },
        { path: 'employees', component: EmployeesComponent },
        { path: '**', redirectTo: 'dashboard' }
      ]
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
