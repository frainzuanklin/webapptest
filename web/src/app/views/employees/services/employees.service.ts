import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from "../../../../environments/environment";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  apiUrl: String;
  constructor(
    private _http: HttpClient,
  ) {
    this.apiUrl = environment.apiUrl + "employees/";
  }
  get(pagina: Number, items: Number, buscar: string): Observable<any> {
    return this._http.get<any>(this.apiUrl + 'get/' + pagina + '/' + items + '/' + encodeURIComponent(buscar), {});
  }
  add(params: Object): Observable<any> {
    return this._http.post<any>(this.apiUrl + 'add', params, {});
  }
  edit(params: Object): Observable<any> {
    return this._http.put<any>(this.apiUrl + 'edit', params, {});
  }
  delete(id: Number): Observable<any> {
    return this._http.delete<any>(this.apiUrl + 'delete/' + id, {});
  }
}
