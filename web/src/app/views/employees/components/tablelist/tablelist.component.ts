import { Component, Input, OnInit, SimpleChange, Output, EventEmitter } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { EmployeesService } from "../../services/employees.service";

export interface EmployeeElement {
  id?: number;
  name: string;
  age: number;
  username: string;
  status?: number;
}

const ELEMENT_DATA: EmployeeElement[] = [];

@Component({
  selector: 'app-tablelist',
  templateUrl: './tablelist.component.html',
  styleUrls: ['./tablelist.component.css']
})
export class TablelistComponent implements OnInit {
  @Output() EdititemEvent = new EventEmitter<any>();
  @Input() add: number|undefined;
  @Input() update: any;
  @Input() delete: any;

  total: number = 0;
  page: number = 0;
  numItems: number = 10;
  buscar: string = "";
  displayedColumns: string[] = ['name', 'age', 'username', "actions"];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  
  constructor(private employeesService: EmployeesService) { }

  ngOnInit(): void {
    this.get();
  }
  ngOnChanges(changes: { [property: string]: SimpleChange }){
      if (changes.add && changes.add.currentValue) {
        this.get();
      } else if (changes.update && changes.update.currentValue !== undefined) {
        this.dataSource.data[Number(changes.update.currentValue.index)].name = changes.update.currentValue.name;
        this.dataSource.data[Number(changes.update.currentValue.index)].age = changes.update.currentValue.age;
        this.dataSource.data[Number(changes.update.currentValue.index)].username = changes.update.currentValue.username;
        this.dataSource._updateChangeSubscription();
        this.update = {};
      } else if (changes.delete && changes.delete.currentValue !== undefined) {
        delete this.dataSource.data[Number(changes.delete.currentValue.index)];
        this.dataSource.data.splice(Number(changes.delete.currentValue.index), 1);
        this.dataSource._updateChangeSubscription();
        this.delete = {};
      }
  }
  pageSelect(page: any) {
    this.page = page.pageIndex;
    this.get();
  }
  get() {
    this.employeesService.get(this.page, this.numItems, this.buscar)
      .subscribe((trae: any) => {
       this.total = trae.total || 0;
       this.dataSource.data = trae.results;
       this.dataSource._updateChangeSubscription();
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    setTimeout(() => { this.get(); }, 500);
  }
  addNewItem(value: any, index: number) {
    value.index = index;
    this.EdititemEvent.emit(value);
  }
}
