import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { EmployeesService } from "./services/employees.service";

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  myform!: FormGroup;
  public addForm: number = 0;
  public updateForm: any;
  public delForm: number = 0;
  public deleteForm: any;

  actions: boolean = true;
  addAction: boolean = false;
  idSelected!: number|undefined;
  indexSelected!: number|undefined;

  nameFormControl = new FormControl('', [
    Validators.required,
  ]);
  ageFormControl = new FormControl('', [
    Validators.required,
  ]);
  usernameFormControl = new FormControl('', [
    Validators.required,
  ]);

  matcher = new MyErrorStateMatcher();

    constructor(private employeesService: EmployeesService) { 
    }

  ngOnInit(): void {
    this.myform = new FormGroup({
      name: this.nameFormControl,
      age: this.ageFormControl,
      username: this.usernameFormControl
    });
  }
  ChooseItem(newItem: any) {
    this.idSelected = newItem.id;
    this.indexSelected = newItem.index;
    this.myform.patchValue({"name": newItem.name, "age": newItem.age, "username": newItem.username });
    this.addAction = true;
    this.actions = false;
  }

  add() {
    if (this.myform.status === "VALID") {
      this.addAction = true;
      let set = {
        name: this.nameFormControl.value,
        age: this.ageFormControl.value,
        username: this.usernameFormControl.value
      };
      this.myform.reset();
      this.employeesService.add(set)
      .subscribe((trae: any) => {
        this.addAction = false;
        this.addForm++;
      });
    }
  }
  edit () {
    if (this.myform.status === "VALID") {
      this.actions = true;
      let set = {
        id: this.idSelected,
        name: this.nameFormControl.value,
        age: this.ageFormControl.value,
        username: this.usernameFormControl.value,
        index: this.indexSelected
      };
      this.idSelected = undefined;
      this.myform.reset();
      this.employeesService.edit(set)
      .subscribe((trae: any) => {
        this.addAction = false;
        this.updateForm = set;
      });
    }
  }

  delete() {
    if (this.idSelected) {
      this.actions = true;
      this.myform.reset();
      this.employeesService.delete(this.idSelected)
      .subscribe((trae: any) => {
        this.deleteForm = {index: this.indexSelected, delForm: this.delForm++};
        this.idSelected = undefined;
        this.indexSelected = undefined;
        this.addAction = false;
      });
    }
  }
  resetAll() {
    this.actions = true;
    this.addAction = false;
    this.myform.reset();
    this.idSelected = undefined;
    this.indexSelected = undefined;
  }
  
}
