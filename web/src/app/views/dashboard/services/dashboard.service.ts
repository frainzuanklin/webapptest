import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from "../../../../environments/environment";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  apiUrl: String;
  constructor(
    private _http: HttpClient,
  ) {
    this.apiUrl = environment.apiUrl + "employees/";
  }
  get(): Observable<any> {
    return this._http.get<any>(this.apiUrl + 'analytics/', {});
  }
}
