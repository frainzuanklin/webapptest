import { Component, OnInit } from '@angular/core';
import { DashboardService } from "./services/dashboard.service";
import { ChartType, ChartOptions } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };
  public pieChartLabels: Label[] = [['']];
  public pieChartData: SingleDataSet = [0];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public alls: number = 0;
  constructor(private dashboardService: DashboardService) { 
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit(): void {
    this.dashboardService.get()
    .subscribe((trae: any) => {
     this.pieChartLabels = trae.labels;
     this.pieChartData = trae.totals;
     this.alls = trae.labels.length + trae.totals.length; 
    });
  }

}
